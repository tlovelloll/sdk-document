{
    "wechat": {
        "app_id": "wx7f620e4e5766e5b0",
        "shares": {
            "sy_title": "体验在高空中来回摇晃的感觉！",
            "sy_img": "https://xcx.youletd.com/img/share/hcxssfr_share.jpg"
        },
        "intersititia_pos_id": "adunit-6603daf93c2ebf23",
        "video_pos_id": "adunit-8905186f5337bfbb",
        "banner_pos_id": {
            "home": "adunit-66cb377c1c752473",
            "game": "adunit-f2158f624f6f851c"
        }
    },
    "vivo": {
        "app_id": "http://source.youletd.com/xcx/h5/common/v105/com.yzxx.hcxssfr.kyx.vivo",
        "intersititial_first_ad": "native",
        "intersititia_pos_id": "f89e5a2b75d14410a5791d85a11d6231",
        "native_intersititial_pos_id": [
            "77b2321205bd46469d2936f11f6cb285"
        ],
        "video_pos_id": "fb7b3e424641448d8b5002501167b43b",
        "banner_first_ad": "native",
        "banner_pos_id": "406179c65e9c48af8a56b2b3a8a9c594",
        "native_banner_pos_id": [
            "103006db420a43f296aab5e2492e7cab"
        ]
    },
    "oppo": {
        "app_id": "30067987",
        "package_name": "com.yzxx.zdxsjjzc.kyx.nearme.gamecenter",
        "banner_pos_id": "98277",
        "video_pos_id": "98278",
        "intersititia_pos_id": "98282",
        "native_banner_pos_id": [
            "98298",
            "98300",
            "98304",
            "98305"
        ],
        "native_intersititial_pos_id": [],
        "intersititial_first_ad": "native",
        "banner_first_ad": "native",
        "native_single_pos_id": [
            "137847",
            "137845",
            "137843",
            "137842"
        ],
        "rec_game_banner_id": "203720",
        "rec_portal_id": "203719",
        "version": "1.0.0"
    },
    "qq": {
        "app_id": "1110683321",
        "box_pos_id": "ab6c414d66ca486007123a07f5c227e4",
        "banner_pos_id": "10e705d3c364aaf5e495743caf783314",
        "video_pos_id": "18dd685d5776a3c0797b0c2fcab653b0",
        "intersititia_pos_id": "ba8ad445b9587f8d28137ef9400a8bde",
        "banner_box_pos_id": "570bd1342bfddb3c77cbaa9a172aad80",
        "shares": {
            "sy_title": "跟随着动感音乐节奏一起摇摆！",
            "sy_img": "s200801152733.jpg"/*tpa=http://xcx.youletd.com/img/jump/s200801152733.jpg*/
        },
        "version": "1.0.0"
    },
    "toutiao": {
        "app_id": "tt360440b9a93e3184",
        "video_pos_id": "hrgrlrqbv271e9fmjn",
        "intersititia_pos_id": "12dae465mfad1d2a01",
        "banner_pos_id": "1051hhebk90dd16473",
        "shares": {
            "sy_title": "体验在高空中来回摇晃的感觉！",
            "sy_img": "https://xcx.youletd.com/img/share/hcxssfr_share.jpg"
        },
        "version": "1.0.0"
    },
    "qutoutiao": {
        "app_id": "a3GRGoh1Lc1m",
        "app_key": "IOBGB3JBIPNLOLXAISjAO2A8CUBgAKdeBXGTj7MxZMTuIxjCIOZKeTjj9i7NFkNC",
        "game_name": "火柴侠伸缩飞人"
    },
    "baidu": {
        "app_id": "4a689b9e4ead44be9b11ae02d53afd92",
        "app_sid": "bf464353",
        "video_pos_id": "6349875",
        "banner_pos_id": "6349873",
        "shares": {
            "sy_title": "我的滑板鞋，时尚最时尚?",
            "sy_img": "https://xcx.youletd.com/img/share/hldld_share.jpg"
        }
    },
    "uc": {
        "app_id": "4a689b9e4ead44be9b11ae02d53afd92",
        "shares": {
            "sy_title": "我的滑板鞋，时尚最时尚?",
            "sy_img": "https://xcx.youletd.com/img/share/hldld_share.jpg"
        },
        "version": "1.0.0"
    },
    "cocos": {
        "app_id": "854584713",
        "video_pos_id": "804975613",
        "banner_pos_id": "903285741",
        "intersititia_pos_id": "158439062",
        "shares": {
            "sy_title": "我的滑板鞋，时尚最时尚?",
            "sy_img": "https://xcx.youletd.com/img/share/hldld_share.jpg"
        }
    },
    "android": {
        "app_id": "yz123456"
    },
    "kwai": {
        "app_id": "ks653303421947775581",
        "video_pos_id": "2300000024_01",
        "shares": {
            "sy_title": "体验在高空中来回摇晃的感觉！",
            "sy_img": "http://xcx.youletd.com/img/share/hcxssfr_share.jpg",
            "sy_icon": "http://xcx.youletd.com/img/share/hcxssfr_share.jpg",
            "sy_desc": "体验在高空中来回摇晃的感觉！"
        },
        "version": "1.0.0"
    },
    "other": {
        "group": "default"
    }
}